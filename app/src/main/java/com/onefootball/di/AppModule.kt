package com.onefootball.di

import android.app.Application
import com.onefootball.OneFootBallApp
import dagger.Binds
import dagger.Module

@Module
abstract class AppModule {
    @Binds
    abstract fun bindContext(app: OneFootBallApp): Application
}