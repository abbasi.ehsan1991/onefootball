package com.onefootball.di

import com.onefootball.data.NewsRepositoryImpl
import com.onefootball.domain.NewsRepository
import com.onefootball.domain.common.ErrorHandler
import com.onefootball.domain.common.ErrorHandlerImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {
    @Binds
    abstract fun bindNewsRepository(newsRepositoryImpl: NewsRepositoryImpl): NewsRepository

    @Binds
    abstract fun bindErrorHandler(errorHandlerImpl: ErrorHandlerImpl): ErrorHandler
}