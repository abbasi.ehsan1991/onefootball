package com.onefootball.di

import com.onefootball.OneFootBallApp
import com.onefootball.presentation.di.ViewModelBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        AndroidInjectionModule::class,
        BuilderModule::class,
        ViewModelBuilder::class,
        TransformerModule::class,
        NetworkModule::class,
        RepositoryModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: OneFootBallApp): Builder

        fun build(): AppComponent
    }

    fun inject(app: OneFootBallApp)
}
