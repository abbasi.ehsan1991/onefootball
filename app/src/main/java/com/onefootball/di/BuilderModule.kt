package com.onefootball.di

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.onefootball.presentation.di.ViewModelKey
import com.onefootball.presentation.news.NewsActivity
import com.onefootball.presentation.news.view.fragment.NewsFragment
import com.onefootball.presentation.news.view.viewmodel.NewsFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module()
abstract class BuilderModule {

    @ContributesAndroidInjector(modules = [NewsActivityModule::class])
    abstract fun bindNesActivity(): NewsActivity

    @Module
    abstract class NewsActivityModule {
        @ContributesAndroidInjector(modules = [NewsFragmentModule::class])
        abstract fun newsFragmentInjector(): NewsFragment
    }

    @Module(includes = [NewsFragmentViewModelModule::class])
    abstract class NewsFragmentModule {
        @Binds
        abstract fun fragment(newsFragment: NewsFragment): Fragment
    }

    @Module
    abstract class NewsFragmentViewModelModule {
        @Binds
        @IntoMap
        @ViewModelKey(NewsFragmentViewModel::class)
        abstract fun bindNewsFragmentViewModel(viewModel: NewsFragmentViewModel): ViewModel
    }
}