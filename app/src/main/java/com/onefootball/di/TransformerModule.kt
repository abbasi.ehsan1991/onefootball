package com.onefootball.di

import com.onefootball.domain.NewsObject
import com.onefootball.domain.executor.AsyncSTranformer
import com.onefootball.domain.executor.STransformer
import com.onefootball.domain.common.Result
import dagger.Module
import dagger.Provides

@Module
object TransformerModule {
    @Provides
    @JvmStatic
    fun bindUserLocationTransfomer(): STransformer<Result<List<NewsObject>>> {
        return AsyncSTranformer()
    }
}