package com.onefootball.presentation.common

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject

abstract class BaseViewHolder<T>(view: View) : RecyclerView.ViewHolder(view) {
    protected val actionSubject = PublishSubject.create<BaseAction>()

    abstract fun bind(data: T)

    fun observe(): Observable<BaseAction> {
        return actionSubject.hide()
    }
}