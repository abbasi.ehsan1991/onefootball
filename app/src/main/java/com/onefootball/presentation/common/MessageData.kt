package com.onefootball.presentation.common

import androidx.annotation.StringRes

data class MessageData(
    val message: String? = null,
    @StringRes
    val resource: Int? = null
)
