package com.onefootball.presentation.common

interface BaseAction {
    fun getType(): ActionType
}
