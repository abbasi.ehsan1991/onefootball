package com.onefootball.presentation.common

import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar

fun <T> LifecycleOwner.observe(liveData: LiveData<T>?, action: (t: T) -> Unit) {
    liveData?.observe(this, Observer { t -> action(t) })
}

fun <T> LifecycleOwner.observeEvent(liveData: LiveData<Event<T>>?, action: (T) -> Unit) {
    liveData?.observe(this, EventObserver(action))
}

fun View.showSnakeBar(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}