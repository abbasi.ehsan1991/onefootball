package com.onefootball.presentation.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.onefootball.domain.common.ErrorModel
import com.onefootball.domain.common.Result
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable

abstract class BaseViewModel(
) : ViewModel() {
    private val disposable = CompositeDisposable()
    private val taggedDisposables = mutableMapOf<String, Disposable>()

    protected val _message = MutableLiveData<Event<MessageData>>()
    val message: LiveData<Event<MessageData>>
        get() = _message

    protected val _loadingData = MutableLiveData<Event<Boolean>>()
    val loadindData: LiveData<Event<Boolean>> = _loadingData

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    fun <T> Result.Error<T>.handleError() {
        when (error) {
            ErrorModel.Network -> _message.value = Event(MessageData("Message base on each error type"))
            else -> {
                //TODO Error message based on each message
            }
        }
    }

    protected fun Disposable.track(tag: String? = null): Disposable {
        disposable.add(this)
        tag?.let {
            dispose(it)
            taggedDisposables[it] = this
        }
        return this
    }

    fun dispose(tag: String) {
        taggedDisposables[tag]?.let {
            it.unTrack()
            it.dispose()
            taggedDisposables.remove(tag)
        }
    }

    private fun Disposable.unTrack() {
        disposable.remove(this)
    }
}