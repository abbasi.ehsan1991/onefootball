package com.onefootball.presentation.common

open class Event<out T>(private val content: T) {
    private var hasBeenHandled = false

    @Synchronized
    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled)
            null
        else {
            hasBeenHandled = true
            content
        }
    }
    fun peekContent(): T = content
}