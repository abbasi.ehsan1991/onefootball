package com.onefootball.presentation.news.view.fragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.onefootball.databinding.FragmentNewsBinding
import com.onefootball.domain.NewsObject
import com.onefootball.presentation.common.MessageData
import com.onefootball.presentation.common.observe
import com.onefootball.presentation.common.observeEvent
import com.onefootball.presentation.common.showSnakeBar
import com.onefootball.presentation.di.ViewModelFactory
import com.onefootball.presentation.news.view.fragment.adapter.NewsAdapter
import com.onefootball.presentation.news.view.viewmodel.NewsFragmentViewModel
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class NewsFragment : DaggerFragment() {

    @Inject
    lateinit var factory: ViewModelFactory

    private lateinit var viewModel: NewsFragmentViewModel
    private lateinit var adapter: NewsAdapter
    private var _binding: FragmentNewsBinding? = null
    private val binding get() = _binding!!

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(NewsFragmentViewModel::class.java)
        with(viewModel) {
            adapter = NewsAdapter { observeActions(it.observe()) }
            observe(newsData, ::observeNewsList)
            observeEvent(showNewsDetail, ::observeShowNewsDetail)
            observeEvent(loadindData, ::observeLoadingState)
            observeEvent(message, ::observeMessage)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNewsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.newsRecyclerView.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun observeLoadingState(isLoading: Boolean) {
        binding.loadingIndicator.visibility =
            if (isLoading)
                View.VISIBLE
            else
                View.GONE
    }

    private fun observeNewsList(news: List<NewsObject>) {
        adapter.addItems(news)
    }

    private fun observeShowNewsDetail(news: NewsObject) {
       startActivity(
            Intent(Intent.ACTION_VIEW, Uri.parse(news.newsLink))
        )
    }

    private fun observeMessage(messageData: MessageData) {
        if (messageData.message != null)
            binding.root.showSnakeBar(messageData.message)
        else if (messageData.resource != null)
            binding.root.showSnakeBar(getString(messageData.resource))
    }

    companion object {
        fun newInstance(): NewsFragment {
            return NewsFragment()
        }
    }
}