package com.onefootball.presentation.news.view.fragment.adapter

import com.onefootball.domain.NewsObject
import com.onefootball.presentation.common.ActionType
import com.onefootball.presentation.common.BaseAction

class NewsDetailAction constructor(val news: NewsObject) : BaseAction {
    override fun getType() = ActionType.NEWS_DETAIL
}