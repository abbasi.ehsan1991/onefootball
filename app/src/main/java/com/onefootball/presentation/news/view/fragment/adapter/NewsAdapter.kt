package com.onefootball.presentation.news.view.fragment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.onefootball.databinding.AdapterNewsBinding
import com.onefootball.domain.NewsObject
import com.onefootball.presentation.common.BaseViewHolder

class NewsAdapter(
    private val listener: ((holder: BaseViewHolder<*>) -> Unit)
) : RecyclerView.Adapter<BaseViewHolder<*>>() {
    private val items = mutableListOf<NewsObject>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
       val view = AdapterNewsBinding.inflate(LayoutInflater.from(parent.context) , parent, false)
        val holder = NewsViewHolder(view)
        listener.invoke(holder)
        return holder
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        if (holder is NewsViewHolder) holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun addItems(news: List<NewsObject>) {
        items.addAll(news)
        notifyDataSetChanged()
    }
}