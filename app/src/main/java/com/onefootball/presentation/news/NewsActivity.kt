package com.onefootball.presentation.news

import android.os.Bundle
import com.onefootball.databinding.ActivityNewsBinding
import com.onefootball.presentation.news.view.fragment.NewsFragment
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity

class NewsActivity : DaggerAppCompatActivity() {

    lateinit var binding: ActivityNewsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = ActivityNewsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState == null)
            supportFragmentManager.beginTransaction()
                .replace(
                    binding.mainFragmentContainer.id,
                    NewsFragment.newInstance(),
                    NewsFragment::class.simpleName
                )
                .commit()
    }
}