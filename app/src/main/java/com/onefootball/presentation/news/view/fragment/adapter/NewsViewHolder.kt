package com.onefootball.presentation.news.view.fragment.adapter

import coil.api.load
import com.onefootball.databinding.AdapterNewsBinding
import com.onefootball.domain.NewsObject
import com.onefootball.presentation.common.BaseViewHolder

class NewsViewHolder(
    private val binding: AdapterNewsBinding
) : BaseViewHolder<NewsObject>(binding.root) {
    override fun bind(data: NewsObject) {
        with(binding) {
            adapterNewsTitleTextView.text = data.title
            adapterNewsResourceNameTextView.text = data.resourceName
            adapterNewsAvatarImageView.load(data.imageURL)
            adapterNewsIconImageView.load(data.resourceURL)

            root.setOnClickListener {
                actionSubject.onNext(NewsDetailAction(data))
            }
        }
    }
}
