package com.onefootball.presentation.news.view.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.onefootball.domain.GetNewsUseCase
import com.onefootball.domain.NewsObject
import com.onefootball.domain.common.Result
import com.onefootball.presentation.common.BaseAction
import com.onefootball.presentation.common.BaseViewModel
import com.onefootball.presentation.common.Event
import com.onefootball.presentation.news.view.fragment.adapter.NewsDetailAction
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class NewsFragmentViewModel @Inject constructor(
    private val getNewsUseCase: GetNewsUseCase,
) : BaseViewModel() {

    private val _newsData = MutableLiveData<List<NewsObject>>()
    val newsData: LiveData<List<NewsObject>>
        get() = _newsData

    val showNewsDetail = MutableLiveData<Event<NewsObject>>()

    init {
        getNews()
    }

    fun observeActions(observable: Observable<BaseAction>) {
        observable.subscribe(
            {
                when (it) {
                    is NewsDetailAction -> showNewsDetail.value = Event(it.news)
                }
            }, {}
        ).track()
    }

    fun getNews() {
        _loadingData.value = Event(true)
        getNewsUseCase.execute()
            .doFinally { _loadingData.value = Event(false) }
            .subscribe({
                when (it) {
                    is Result.Success -> {
                        _newsData.value = it.data
                    }
                    is Result.Error -> it.handleError()
                }
            }, {
                println(it)
            }).track()
    }
}