package com.onefootball.domain.executor

import io.reactivex.rxjava3.core.SingleTransformer

abstract class STransformer<T> : SingleTransformer<T, T>