package com.onefootball.domain.executor

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleSource
import io.reactivex.rxjava3.schedulers.Schedulers


class AsyncSTranformer<T>(
    private val threadExcutor: Scheduler = Schedulers.io(),
    private val postExceptionThread: Scheduler = AndroidSchedulers.mainThread()
) : STransformer<T>() {
    override fun apply(upstream: Single<T>): SingleSource<T> =
        upstream.subscribeOn(threadExcutor)
            .observeOn(postExceptionThread)
}