package com.onefootball.domain

import com.onefootball.domain.common.ErrorHandler
import com.onefootball.domain.common.Result
import com.onefootball.domain.executor.STransformer
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetNewsUseCase @Inject constructor(
    private val repository: NewsRepository,
    private val transformer: STransformer<Result<List<NewsObject>>>,
    private val errorHandler: ErrorHandler
) {
    fun execute(): Single<Result<List<NewsObject>>> {
        return repository.getNews()
            .map { Result.Success(it) }
            .compose(transformer)
            .onErrorReturn { throwable ->
                Result.Error(errorHandler.getMessage(throwable))
            }
    }
}