package com.onefootball.domain

data class NewsObject(
    val title: String,
    val imageURL: String,
    val resourceName: String,
    val resourceURL: String,
    val newsLink: String
)