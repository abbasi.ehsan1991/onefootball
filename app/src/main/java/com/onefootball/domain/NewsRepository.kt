package com.onefootball.domain

import io.reactivex.rxjava3.core.Single

interface NewsRepository {
    fun getNews(): Single<List<NewsObject>>
}