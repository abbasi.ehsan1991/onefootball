package com.onefootball.domain.common

import retrofit2.HttpException
import java.io.IOException
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class ErrorHandlerImpl @Inject constructor() : ErrorHandler {

    override fun getMessage(throwable: Throwable): ErrorModel {
        return when (throwable) {
            is TimeoutException,
            is ConnectException,
            is UnknownHostException,
            is IOException -> ErrorModel.Network
            is HttpException -> {
                when (throwable.code()) {
                    HttpURLConnection.HTTP_NOT_FOUND -> ErrorModel.NotFound
                    HttpURLConnection.HTTP_FORBIDDEN -> ErrorModel.AccessDenied
                    else -> ErrorModel.Unknown
                }
            }
            else -> ErrorModel.Unknown
        }
    }
}

interface ErrorHandler {
    fun getMessage(throwable: Throwable): ErrorModel
}