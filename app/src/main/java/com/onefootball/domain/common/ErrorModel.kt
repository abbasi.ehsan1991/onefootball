package com.onefootball.domain.common

sealed class ErrorModel {
    object Network : ErrorModel()
    object NotFound : ErrorModel()
    object AccessDenied : ErrorModel()
    object ServiceUnavailable : ErrorModel()
    object Unknown : ErrorModel()
}