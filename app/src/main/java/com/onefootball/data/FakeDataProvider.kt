package com.onefootball.data

import android.app.Application
import com.onefootball.data.entity.News
import io.reactivex.rxjava3.core.Single
import org.json.JSONArray
import org.json.JSONObject
import java.nio.charset.Charset
import javax.inject.Inject

class FakeDataProvider @Inject constructor(
   private val application: Application
) {

    fun getFakeNews(): Single<List<News>> {
        val inputStream = application.assets.open("news.json")
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        val fakeNews = parseJsonString(
            buffer.toString(Charset.defaultCharset())
        )
        return Single.just(fakeNews)
    }

    private fun parseJsonString(jsonString: String): List<News> {
        val mainObject = JSONObject(jsonString)
        val newsItems = mutableListOf<News>()
        val newsArray = mainObject.getJSONArray("news")
        newsArray.forEach { newsObject ->
            val title = newsObject.getString("title")
            val imageURL = newsObject.getString("image_url")
            val resourceName = newsObject.getString("resource_name")
            val resourceURL = newsObject.getString("resource_url")
            val newsLink = newsObject.getString("news_link")

            newsItems.add(
                News(
                    title = title,
                    imageURL = imageURL,
                    resourceName = resourceName,
                    resourceURL = resourceURL,
                    newsLink = newsLink
                )
            )
        }
        return newsItems
    }

    private fun JSONArray.forEach(jsonObject: (JSONObject) -> Unit) {
        for (index in 0 until this.length()) jsonObject(this[index] as JSONObject)
    }
}
