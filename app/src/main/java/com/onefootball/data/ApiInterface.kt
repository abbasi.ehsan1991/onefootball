package com.onefootball.data

import com.onefootball.data.entity.News
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface ApiInterface {
    @GET("/mock/api/news")
    fun getNews(): Single<List<News>>
}