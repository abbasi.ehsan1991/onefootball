package com.onefootball.data.entity

import com.onefootball.domain.NewsObject

data class News(
    val title: String,
    val imageURL: String,
    val resourceName: String,
    val resourceURL: String,
    val newsLink: String
)

fun List<News>.mapToListOfNewsObject() = map {
    it.mapToNewsObject()
}

fun News.mapToNewsObject(): NewsObject {
    return NewsObject(
        title = title,
        imageURL = imageURL,
        resourceName = resourceName,
        resourceURL = resourceURL,
        newsLink = newsLink
    )
}