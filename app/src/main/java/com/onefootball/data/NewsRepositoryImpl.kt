package com.onefootball.data

import com.onefootball.data.entity.mapToListOfNewsObject
import com.onefootball.domain.NewsObject
import com.onefootball.domain.NewsRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val apiInterface: ApiInterface,
    private val fakeData: FakeDataProvider
) : NewsRepository {
    /**
     * This is a fake reposnse that we are using, But We
     * Can replace it very easy with our [apiInterface]
     * And call a real API with it.
     */
    override fun getNews(): Single<List<NewsObject>> {
        return fakeData.getFakeNews().map {
            it.mapToListOfNewsObject()
        }
    }
}
