package com.onefootball.domain

import com.nhaarman.mockitokotlin2.mock
import com.onefootball.domain.common.Result
import com.onefootball.common.TestSTransformer
import com.onefootball.common.fakeNewsListObject
import com.onefootball.common.fakeThrowableError
import com.onefootball.domain.common.ErrorHandler
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class GetNewsUseCaseTest {
    private val repository: NewsRepository = mock()
    private val transformer: TestSTransformer<Result<List<NewsObject>>> = TestSTransformer()
    private val errorHandler: ErrorHandler = mock()
    private lateinit var useCase: GetNewsUseCase

    @Before
    fun setup() {
        useCase = GetNewsUseCase(repository, transformer, errorHandler)
    }

    @Test
    fun getNews_onSuccess_shouldReturnNewsInfo() {
        val fakeResponse = fakeNewsListObject()
        Mockito.`when`(repository.getNews()).thenReturn(Single.just(fakeResponse))

        useCase.execute()
            .test()
            .assertComplete()
            .assertValue(Result.Success(fakeResponse))

        Mockito.verify(repository).getNews()
    }

    @Test
    fun getNews_onError_shouldReturnError() {
        Mockito.`when`(repository.getNews()).thenReturn(Single.error(fakeThrowableError()))

        useCase.execute()
            .test()
            .assertNotComplete()
            .assertError(Throwable::class.java)

        Mockito.verify(repository).getNews()
    }
}