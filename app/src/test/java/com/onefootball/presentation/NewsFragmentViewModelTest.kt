package com.onefootball.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.onefootball.common.fakeNewsListObject
import com.onefootball.domain.GetNewsUseCase
import com.onefootball.domain.NewsObject
import com.onefootball.domain.common.ErrorModel
import com.onefootball.domain.common.Result
import com.onefootball.presentation.common.BaseAction
import com.onefootball.presentation.common.Event
import com.onefootball.presentation.common.MessageData
import com.onefootball.presentation.news.view.fragment.adapter.NewsDetailAction
import com.onefootball.presentation.news.view.viewmodel.NewsFragmentViewModel
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.subjects.PublishSubject
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class NewsFragmentViewModelTest {

    private val getNewsUseCase: GetNewsUseCase = mock()
    private lateinit var viewModel: NewsFragmentViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun getNews_onSuccess_shouldSetNewsData() {
        Mockito.`when`(getNewsUseCase.execute())
            .thenReturn(Single.just(Result.Success(fakeNewsListObject())))
        val fakeNewsDataObserver = mock<(List<NewsObject>) -> Unit>()
        val fakeLoadingDataObserver = mock<(Event<Boolean>) -> Unit>()

        viewModel = NewsFragmentViewModel(getNewsUseCase)
        viewModel.getNews()

        viewModel.newsData.observeForever(fakeNewsDataObserver)
        viewModel.loadindData.observeForever(fakeLoadingDataObserver)

        Mockito.verify(fakeNewsDataObserver).invoke(any())
        Assert.assertEquals(false, viewModel.loadindData.value!!.peekContent())
        Mockito.verify(fakeLoadingDataObserver).invoke(any())
    }

    @Test
    fun getNews_onError_shouldSetErrorData(){
        Mockito.`when`(getNewsUseCase.execute())
            .thenReturn(Single.just(Result.Error(ErrorModel.Network)))
        val fakeErrorDataObserver = mock<(Event<MessageData>) -> Unit>()

        viewModel = NewsFragmentViewModel(getNewsUseCase)
        viewModel.message.observeForever(fakeErrorDataObserver)

        Mockito.verify(fakeErrorDataObserver).invoke(any())
        Assert.assertEquals(false, viewModel.loadindData.value!!.peekContent())
    }

    @Test
    fun observeClickActions_whenUseClickedOnEachNews_shouldShowDetails() {
        Mockito.`when`(getNewsUseCase.execute())
            .thenReturn(Single.just(Result.Success(fakeNewsListObject())))
        val fakeClicksObserver = PublishSubject.create<BaseAction>()
        val fakeClickAction = NewsDetailAction(fakeNewsListObject()[0])
        val fakeNewsDetailObserver = mock<(Event<NewsObject>)->Unit>()

        viewModel = NewsFragmentViewModel(getNewsUseCase)
        viewModel.showNewsDetail.observeForever(fakeNewsDetailObserver)
        viewModel.observeActions(fakeClicksObserver)
        fakeClicksObserver.onNext(fakeClickAction)

        Mockito.verify(fakeNewsDetailObserver).invoke(any())
        Assert.assertEquals(fakeNewsListObject()[0], viewModel.showNewsDetail.value!!.peekContent())
    }
}