package com.onefootball.data

import com.onefootball.common.fakeNewsList
import com.onefootball.common.fakeNewsListObject
import com.onefootball.common.fakeThrowableError
import com.onefootball.domain.NewsRepository
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class NewsRepositoryTest {

    @Mock
    private var apiInterface: ApiInterface = Mockito.mock(ApiInterface::class.java)

    @Mock
    private val fakeDataProvider: FakeDataProvider = Mockito.mock(FakeDataProvider::class.java)

    private lateinit var repository: NewsRepository


    @Before
    fun setup() {
        repository = NewsRepositoryImpl(apiInterface, fakeDataProvider)
    }

    @Test
    @Ignore("Because we are using fake response we don't need this test now, After uising real API remove ignore annotation")
    fun getNews_onSuccess_shouldReturnQuizInfo() {
        Mockito.`when`(apiInterface.getNews()).thenReturn(Single.just(fakeNewsList()))
        repository.getNews()
            .test()
            .assertNoErrors()
            .assertComplete()
            .assertValue(fakeNewsListObject())
    }

    @Test
    @Ignore("Because we are using fake response we don't need this test now, After uising real API remove ignore annotation")
    fun getNews_onError_shouldReturnError() {
        Mockito.`when`(apiInterface.getNews())
            .thenReturn(Single.error(fakeThrowableError()))
        repository.getNews()
            .test()
            .assertError(Throwable::class.java)
            .assertNotComplete()
    }
}