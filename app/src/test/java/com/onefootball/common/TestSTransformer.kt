package com.onefootball.common

import com.onefootball.domain.executor.STransformer
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleSource

class TestSTransformer<T>: STransformer<T>() {
    override fun apply(upstream: Single<T>): SingleSource<T> {
        return upstream
    }
}