package com.onefootball.common

import com.onefootball.data.entity.News
import com.onefootball.data.entity.mapToNewsObject


fun fakeThrowableError() = Throwable("Fake error happened!")

fun fakeNewsListObject() = fakeNewsList().map { it.mapToNewsObject() }

fun fakeNewsList(): List<News> {
   return listOf<News>(
        News(
            title = "The 5 players who could be the next Messi or Ronaldo",
            imageURL = "https://image-service.onefootball.com/crop/face?h=810&amp;image=https%3A%2F%2Fwp-images.onefootball.com%2Fwp-content%2Fuploads%2Fsites%2F10%2F2019%2F08%2FFIFA-Ballon-dOr-Gala-2014-1566312341-1024x683.jpg&amp;q=25&amp;w=1080",
            resourceName = "Onefootball",
            resourceURL = "https://images.onefootball.com/blogs_logos/circle_onefootball.png",
            newsLink = "https://onefootball.com/en/news/the-5-players-who-could-be-the-next-messi-or-ronaldo-en-26880141?variable=20190822"
        )
    )
}