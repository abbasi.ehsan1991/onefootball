# OneFootball Task

About the application:
At this repository I tried to show my skills on different parts like architecture ans so on.
Besacuse of that some parts may not be necessary at this stage but I tried to develop this application
With a view of completed and maintable application.

For Example all requirements for real API is provided at this repository and by a simple change we can
Call real API instead of fake response that is used for this stage.

I tried to write unit-tests for different parts of the application but because of time limit their not
Complete.

This Android application developed based on **MVVM** and **Clean Architecture** on 3 modules:
(because of time limit I didn't create seperated modules for this application for our modules
But in the future we can do that very easy.)

I used DaggerAndroid for DI (we can use Dagger2 or Hilt based on our requirements in the future)

1.  Presenter
2.  Data
3.  Domain


## Libraries used:
*  RX Java and Android
*  Dagger Android
*  Coil
*  Retrofit
*  Jetpack components like
    1.  Data Binding
    2.  Live Data
    3.  ViewModel


## Language:
    Kotlin



